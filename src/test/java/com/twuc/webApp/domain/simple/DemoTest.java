package com.twuc.webApp.domain.simple;

import com.twuc.webApp.domain.simple.Person.Person;
import com.twuc.webApp.domain.simple.Person.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DemoTest {
    @Autowired
    private PersonRepository repository;
    @Autowired
    private EntityManager em;

    @Test
    void should_return_no_person() {
        assertFalse(repository.findById(1L).isPresent());
    }

    @Test
    void should_return_person_with_id_is_1() {
        final Person p = new Person(1L, "lei", "liu");
        repository.save(p);
        assertTrue(repository.findById(1L).isPresent());
    }

    @Test
    void should_return_person_with_id_is_1_and_console_insert_sql() {
        final Person p = new Person(1L, "lei", "liu");
        repository.save(p);
        em.flush();
        em.clear();
        assertTrue(repository.findById(1L).isPresent());
    }
}
